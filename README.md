# File Cascade

Utility to load file with the best matched name. i.e. you asks to load "path/to/file.dev.v1.json", but only "path/to/file.dev.json" is present. This utility will load "path/to/file.dev.json". It is useful for different environment based configs.

## Install

```sh
$ npm install file-cascade
```

## Usage

```js
import fileCascade from "file-cascade";

// by default it load file relative to process.cwd() and split filename by a dot - '.'
let data;
data = fileCascade.readFileSync ('data/numbers/numbers.even.10.txt');       // load numbers.even.txt
data = await fileCascade.readFile ('data/numbers/numbers.even.10.txt');     // load numbers.even.txt

// split by '_'
data = fileCascade.readFileSync ('data/numbers/numbers_even_10.txt', '_');  // load numbers.txt

console.log (`data: ${data}`);
console.log (`baseDir: ${fileCascade.baseDir}`);
console.log (`lastLoadedFile: ${fileCascade.lastLoadedFile}`);
```


```js
import {FileCascade} from "file-cascade";

let fc = new FileCascade ();
fc.defaultSplitBy = '_'
fc.baseDir = '/usr/data/';

let data;
data = fc.readFileSync ('config/env_development_v1.yaml');
```


