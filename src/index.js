import fs from "fs";
import * as path from "path";

export class FileCascade
{
	/** @type {String} **/		baseDir;
	/** @type {String} **/		defaultSplitBy;
	/** @type {String} **/		lastLoadedFile;


	constructor ()
	{
		this.baseDir		= process.cwd()+'/';
		this.defaultSplitBy	= '.';
		this.lastLoadedFile	= '';
	}


	/** Split filename into array of filenames to load queue
	 * @param {String[] | String} filename
	 * @param {String} splitBy
	 * @return {String[]}
	 */
	splitFileName (filename, splitBy=this.defaultSplitBy)
	{
		if (filename instanceof Array) return filename;

		let pathParts	= path.parse (filename);
		let nameParts	= pathParts.name.split (splitBy);
		let files		= [];

		switch (pathParts.dir){
			case '/':
			case '':
				break;
			default:
				pathParts.dir += '/';
				break;
		}

		for (let i = nameParts.length-1; i >= 0; --i) {
			files.push (`${pathParts.dir}${nameParts.slice(0, i+1).join(splitBy)}${pathParts.ext}`);
		}

		return files;
	}


	/** Find first file from provided list which is exists
	 * @param {String[] | String} files
	 * @param {String} splitBy
	 * @return {String | false} filepath
	 */
	findFirst (files, splitBy=this.defaultSplitBy)
	{
		if (!(files instanceof Array)) files = this.splitFileName(files, splitBy);

		for (let i = 0; i < files.length; i++) {
			if (fs.existsSync(this.#fullPath(files[i]))) return files[i];
		}

		return false;
	}


	/** Relative filename to absolute path
	 * @param {String} file
	 * @return {String} filepath
	 */
	#fullPath (file)
	{
		return this.baseDir + file;
	}


	/** Apply provided handler to first available file
	 * @param {String[] | String} files
	 * @param {Function} handler
	 * @param {String} splitBy
	 * @return {Promise<void>}
	 */
	processFile (files, handler, splitBy=this.defaultSplitBy)
	{
		let file = this.findFirst (files, splitBy);
		this.lastLoadedFile = '';

		if (file){
			this.lastLoadedFile = file;
			return handler (this.#fullPath(file), file);
		}

		throw new Error(`Unable to process ${files}`);
	}


	/** Import module
	 * @param {String[] | String} files
	 * @param {String} splitBy
	 * @return {Promise<Object>}
	 */
	async import (files, splitBy=this.defaultSplitBy)
	{
		let handler = async function (filepath, file){
			return (await import(`file://${filepath}`));
		}

		return this.processFile (files, handler, splitBy);
	}


	/** Import default export from module
	 * @param {String[] | String} files
	 * @param {String} splitBy
	 * @return {Promise<Class>}
	 */
	async importDefault (files, splitBy=this.defaultSplitBy)
	{
		let handler = async (filepath, file) => {
			return (await import(`file://${filepath}`)).default;
		}

		return this.processFile (files, handler, splitBy);
	}


	/** Read content from file asynchronously
	 * @param {String[] | String} files
	 * @param {String} splitBy
	 * @return {Promise<String>}
	 */
	async readFile (files, splitBy=this.defaultSplitBy)
	{
		return new Promise((resolve, reject) => {

			let handler = (filepath, file) => {
				return fs.readFile (filepath, 'utf8', (err, data) => {
					if (err) reject (err);
					resolve (data);
				});
			}

			try {
				this.processFile (files, handler, splitBy);
			} catch (e) {
				reject (e);
			}
		});
	}


	/** Read content from file
	 * @param {String[] | String} files
	 * @param {String} splitBy
	 * @return {String}
	 */
	readFileSync (files, splitBy=this.defaultSplitBy)
	{
		let handler = (filepath, file) => {
			return fs.readFileSync(filepath, 'utf8').toString();
		}

		return this.processFile (files, handler, splitBy);
	}

}


let fileCascade = new FileCascade();
export default fileCascade;