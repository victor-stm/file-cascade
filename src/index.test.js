import test from 'ava';
import fileCascade, {FileCascade} from "./index.js";

let samples;


//---------------------------------------------------------------------
// lastLoadedFile
//---------------------------------------------------------------------

test (`lastLoadedFile ''`, t => {
	t.is (fileCascade.lastLoadedFile, '');
});


samples = [
	// input , output
	['data/numbers/numbers.even.10.txt',			'data/numbers/numbers.even.txt'],		// not exists
	['data/numbers/numbers.even.hello.txt',			'data/numbers/numbers.even.txt'],		// not exists
	['data/numbers/numbers.complex.txt',			'data/numbers/numbers.txt'],			// not exists
	['data/numbers/numbers.odd.2.txt',				'data/numbers/numbers.odd.txt'],		// not exists
	['data/numbers/numbers.even.txt',				'data/numbers/numbers.even.txt'],
	['data/numbers/numbers.even.2.txt',				'data/numbers/numbers.even.2.txt'],
];

samples.forEach (sample => {
	test (`lastLoadedFile: (${sample[0]}) → ${sample[1]}`, t => {
		fileCascade.readFileSync (sample[0]);
		t.is (
			fileCascade.lastLoadedFile,
			sample[1]
		)
	});
});


//---------------------------------------------------------------------
// splitFileName
//---------------------------------------------------------------------

samples = [
	// input , output
	['/a/b/file.js',							['/a/b/file.js']],
	['/a/b/file.name.js',						['/a/b/file.name.js', '/a/b/file.js']],
	['/a/b/file.name.optional.parts.yaml',		['/a/b/file.name.optional.parts.yaml', '/a/b/file.name.optional.yaml', '/a/b/file.name.yaml', '/a/b/file.yaml']],
	['/file.js', 								['/file.js']],
	['file.name.js',							['file.name.js', 'file.js']],
	['data/some.file.json',						['data/some.file.json', 'data/some.json']],
];

samples.forEach (sample => {
	test (`splitFileName: ${sample[0]}`, t => {
		t.deepEqual (
			fileCascade.splitFileName (sample[0]),
			sample[1]
		)
	});
});


samples = [
	// input, splitBy, output
	['/a/b/file.name_optional_parts.yaml',		'_',		['/a/b/file.name_optional_parts.yaml', '/a/b/file.name_optional.yaml', '/a/b/file.name.yaml']],
];

samples.forEach (sample => {
	test (`splitFileName: (${sample[0]}, ${sample[1]})`, t => {
		t.deepEqual (
			fileCascade.splitFileName (sample[0], sample[1]),
			sample[2]
		)
	});
});


//---------------------------------------------------------------------
// findFirst
//---------------------------------------------------------------------

samples = [
	// input , output
	['data/some.file.json',							'data/some.file.json'],
	['data/some.file.with.optional.parts.json',		'data/some.file.with.json'],
];

samples.forEach (sample => {
	test (`findFirst: (${sample[0]}) → ${sample[1]}`, t => {
		t.is (
			fileCascade.findFirst (sample[0]),
			sample[1]
		)
	});
});


samples = [
	// input , splitBy output
	['data/figure.yaml',						'_',	'data/figure.yaml'],
	['data/figure_blue_circle.yaml',			'_',	'data/figure_blue_circle.yaml'],
	['data/figure_blue_rectangle.yaml',			'_',	'data/figure_blue.yaml'],
	['data/figure_red_circle.yaml',				'_',	'data/figure.yaml'],
	['data/figure_blue_circle.yaml',			'__',	'data/figure_blue_circle.yaml'],
	['data/figure__blue__circle.yaml',			'__',	'data/figure.yaml'],
];

samples.forEach (sample => {
	test (`findFirst: (${sample[0]}, ${sample[1]}) → ${sample[2]}`, t => {
		t.is (
			fileCascade.findFirst (sample[0], sample[1]),
			sample[2]
		)
	});
});


//---------------------------------------------------------------------
// readFile & readFileSync
//---------------------------------------------------------------------

samples = [
	// input , output
	['data/numbers/numbers.even.10.txt',			'2468'],		// not exists
	['data/numbers/numbers.even.hello.txt',			'2468'],		// not exists
	['data/numbers/numbers.complex.txt',			'0123456789'],	// not exists
	['data/numbers/numbers.odd.2.txt',				'13579'],		// not exists
	['data/numbers/numbers.even.txt',				'2468'],
	['data/numbers/numbers.even.2.txt',				'2'],
	['data/numbers/numbers.odd.1.txt',				'1'],
	['data/numbers/numbers.zero.txt',				'0'],
	['data/numbers/numbers.txt',					'0123456789'],
];

samples.forEach (sample => {
	test (`readFile*: (${sample[0]}) → ${sample[1]}`, async t => {
		t.is (
			fileCascade.readFileSync (sample[0]),
			sample[1]
		)
		t.is (
			await fileCascade.readFile (sample[0]),
			sample[1]
		)
	});
});


test (`readFileSync: ('data/missed.file.txt') → throws Error`, async t => {

	let fileCascade = new FileCascade();	// create local instance to test lastLoadedFile

	fileCascade.lastLoadedFile = 'not empty string';
	t.not (fileCascade.lastLoadedFile, '');
	t.throws (()=>{fileCascade.readFileSync ('data/missed.file.txt')});
	t.is (fileCascade.lastLoadedFile, '');

	fileCascade.lastLoadedFile = 'not empty string';
	t.not (fileCascade.lastLoadedFile, '');
	await t.throwsAsync (fileCascade.readFile ('data/missed.file.txt'));
	t.is (fileCascade.lastLoadedFile, '');
});


//---------------------------------------------------------------------
// import
//---------------------------------------------------------------------

test (`importDefault: ('data/js/export.[many].js')`, async t => {
	let res = await fileCascade.importDefault ('./data/js/export.many.js');
	t.is (res, 'file js/test.js')
});

test (`import: ('data/js/export.many.values.[for.all].js')`, async t => {
	let res = await fileCascade.import ('data/js/export.many.values.for.all.js');
	t.is (res.value, 'file js/test.many.exports.js')
	t.is (res.a, 'A');
	t.is (res.b, 'B');
});


